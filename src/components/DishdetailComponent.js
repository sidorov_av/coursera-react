import React, { Component } from 'react';
import { LocalForm, Control, Errors } from 'react-redux-form';
import { Card, CardBody, CardTitle, CardText, CardImg, Breadcrumb, BreadcrumbItem,
    Modal, ModalHeader, ModalBody, Button, Row, Label, Col } from 'reactstrap';
import { Link } from 'react-router-dom';
import { Loading } from './LoadingComponent';
import { baseUrl } from '../shared/baseUrl';
import { FadeTransform, Fade, Stagger } from 'react-animation-components';

const maxLength = len => val => (!val || (val.length <= len));
const minLength = len => val => (val && (val.length >= len));

class CommentForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isModalOpen: false,
        };

        this.toggleModal = this.toggleModal.bind(this);
    }

    toggleModal() {
        this.setState(prevState => ({
            isModalOpen: !prevState.isModalOpen,
        }))
    }

    handleSubmit(values) {
        this.props.postComment(this.props.dishId, values.rating, values.author, values.comment);
    }

    render() {

        const { isModalOpen } = this.state;

        return (
            <div>
                <Button outline color='secondary' onClick={this.toggleModal}>
                    <span className='fa fa-pencil' />
                    Submit Comment
                </Button>
                <Modal isOpen={!!isModalOpen} toggle={this.toggleModal}>
                    <ModalHeader toggle={this.toggleModal}>
                        Submit Comment
                    </ModalHeader>
                    <ModalBody>
                        <LocalForm onSubmit={values => this.handleSubmit(values)}>
                            <Row className='form-group'>
                                <Label htmlFor='.rating' md={12}>Rating</Label>
                                <Col md={12}>
                                    <Control.select
                                        model='.rating'
                                        className='form-control'
                                        id='rating'
                                        name='rating'>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </Control.select>
                                </Col>
                            </Row>
                            <Row className='form-group'>
                                <Label htmlFor='.author' md={12}>Your Name</Label>
                                <Col md={12}>
                                    <Control.text
                                        model='.author'
                                        className='form-control'
                                        id='author'
                                        name='author'
                                        placeholder='Your Name'
                                        validators={{
                                            minLength: minLength(3), maxLength: maxLength(15)
                                        }} />
                                    <Errors
                                        className='text-danger'
                                        model='.author'
                                        show='touched'
                                        messages={{
                                            minLength: 'It should be at least 3 characters',
                                            maxLength: 'It should e max 15 characters'
                                        }} />
                                </Col>
                            </Row>
                            <Row className='form-group'>
                                <Label htmlFor='.comment' md={12}>Comment</Label>
                                <Col md={12}>
                                    <Control.textarea
                                        model='.comment'
                                        className='form-control'
                                        id='comment'
                                        name='comment'
                                        rows={6} />
                                </Col>
                            </Row>
                            <Row className='form-group'>
                                <Col md={12}>
                                    <Button type='submit' color='primary'>Submit</Button>
                                </Col>
                            </Row>
                        </LocalForm>
                    </ModalBody>
                </Modal>
            </div>
        )
    }
}


const RenderDish = (props) => {
    if (props.isLoading) {
        return (
            <div className="container">
                <div className="row">
                    <Loading/>
                </div>
            </div>
        );
    } else if (props.errMess) {
        return (
            <div className="container">
                <div className="row">
                    <h4>{props.errMess}</h4>
                </div>
            </div>
        );
    } else if (props.dish != null) {
        return (
            <FadeTransform
                in
                transformProps={{
                    exitTransform: 'scale(0.5) translateY(-50%)'
                }}>
                <Card>
                    <CardImg top src={baseUrl + props.dish.image} alt={props.dish.name} />
                    <CardBody>
                        <CardTitle>{props.dish.name}</CardTitle>
                        <CardText>{props.dish.description}</CardText>
                    </CardBody>
                </Card>
            </FadeTransform>
        );
    } else {
        return (
            <div></div>
        );
    }
};

const RenderComments = ({comments, postComment, dishId}) => {
    if(comments != null) {
        return (
            <div>
                <h4>Comments</h4>
                <ul className="comments list-unstyled">
                    <Stagger in delay={200}>
                        {comments.map((comment) => {
                            return (
                                <Fade in key={comment.id}>
                                    <li>
                                        <p>{comment.comment}</p>
                                        <p>-- {comment.author} , {new Intl.DateTimeFormat('en-US', { year: 'numeric', month: 'short', day: '2-digit'}).format(new Date(Date.parse(comment.date)))}</p>
                                    </li>
                                </Fade>
                            );
                        })}
                    </Stagger>
                </ul>
                <CommentForm dishId={dishId} postComment={postComment} />
            </div>
        )

    } else {
        return (
            <div></div>
        )
    }
};

const DishDetail = (props) => {
    if (!props.dish) return (<div/>);


    console.log('DishDetail',props);

    return (
        <div className="container">
            <div className="row">
                <Breadcrumb>
                    <BreadcrumbItem><Link to="/menu">Menu</Link></BreadcrumbItem>
                    <BreadcrumbItem active>{props.dish.name}</BreadcrumbItem>
                </Breadcrumb>
                <div className="col-12">
                    <h3>{props.dish.name}</h3>
                    <hr />
                </div>
            </div>
            <div className="row">
                <div className="col-12 col-md-5 m-1">
                    <RenderDish dish={props.dish} />
                </div>
                <div className="col-12 col-md-5 m-1">
                    <RenderComments comments={props.comments}
                                    postComment={props.postComment}
                                    dishId={props.dish.id}
                    />
                </div>
            </div>
        </div>
    )
};

export default DishDetail;